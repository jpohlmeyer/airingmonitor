package com.github.jpohlmeyer.airingmonitor.data.model

data class AiringForecastContainer(
    val airingForecast: List<AiringForecastData>,
    val windowInfo: WindowInfoData
)