package com.github.jpohlmeyer.airingmonitor.data.api.http

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.api.AiringForecastApi
import com.github.jpohlmeyer.airingmonitor.data.api.ApiException
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastContainer
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastData
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.suspendCancellableCoroutine
import java.lang.reflect.Type
import javax.inject.Inject


class AiringForecastApiImpl @Inject constructor(
    private val volleyHTTPClient: VolleyHTTPClient,
    @ApplicationContext applicationContext: Context
) : AiringForecastApi {

    private val gson = Gson()

    private val sharedPreferences: SharedPreferences
    private val settingsHubAdress: String
    private val defaultBaseUrl: String

    init {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        settingsHubAdress = applicationContext.resources.getString(R.string.settings_hub_address)
        defaultBaseUrl = applicationContext.resources.getString(R.string.default_hub_host)
    }

    private suspend fun updateSingleLatestData() : AiringForecastContainer {
        return suspendCancellableCoroutine { continuation ->
            val baseUrl = sharedPreferences.getString(settingsHubAdress, defaultBaseUrl)!!
            val url = "http://$baseUrl/airing_forecast"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                { response: String ->
                    val airingForecastIntermediate: AiringForecastJsonIntermediate = gson.fromJson(response, AiringForecastJsonIntermediate::class.java)
                    val type: Type =
                        TypeToken.getParameterized(List::class.java, AiringForecastData::class.java).type
                    val airingForecastData: List<AiringForecastData> = gson.fromJson(airingForecastIntermediate.airingForecast.toString(), type)
                    val airingForecast = AiringForecastContainer(airingForecastData, airingForecastIntermediate.windowInfo)
                    continuation.resumeWith(Result.success(airingForecast))
                },
                { error ->
                    val msg = if (error.networkResponse == null) {
                        "Data request failed: Network response is null"
                    } else {
                        "Data request failed: " + error.message
                    }
                    continuation.cancel(ApiException(msg))
                })
            volleyHTTPClient.addRequestToQueue(stringRequest)
        }
    }

    override suspend fun fetchAiringForecast(): AiringForecastContainer {
        return updateSingleLatestData()
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class AiringForecastApiModule {
    @Binds
    abstract fun airingForecastData(airingForecastApiImpl: AiringForecastApiImpl) : AiringForecastApi
}