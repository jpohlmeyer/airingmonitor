package com.github.jpohlmeyer.airingmonitor.data.model

class WeatherForecastData(
    val station_id: String,
    val current_temperature: Double,
    val current_humidity: Double,
    val current_dewpoint: Double,
    val current_precipitation: Double,
    val current_timestamp: Long,
    val forecast_made: Long,
    val current_forecasted_precipitation_probability: Double,
    val current_forecasted_wind_speed: Double,
    val current_forecasted_wind_gusts: Double,
    val current_forecasted_wind_direction: Double,
    val forecast_24h_temperature: DoubleArray,
    val forecast_24h_humidity: DoubleArray,
    val forecast_24h_dewpoint: DoubleArray,
    val forecast_24h_precipitation_total: DoubleArray,
    val forecast_24h_precipitation_probability: IntArray,
    val forecast_24h_wind_speed: DoubleArray,
    val forecast_24h_wind_gusts: DoubleArray,
    val forecast_24h_wind_direction: IntArray
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WeatherForecastData

        if (station_id != other.station_id) return false
        if (current_temperature != other.current_temperature) return false
        if (current_humidity != other.current_humidity) return false
        if (current_dewpoint != other.current_dewpoint) return false
        if (current_precipitation != other.current_precipitation) return false
        if (current_timestamp != other.current_timestamp) return false
        if (forecast_made != other.forecast_made) return false
        if (current_forecasted_precipitation_probability != other.current_forecasted_precipitation_probability) return false
        if (current_forecasted_wind_speed != other.current_forecasted_wind_speed) return false
        if (current_forecasted_wind_gusts != other.current_forecasted_wind_gusts) return false
        if (current_forecasted_wind_direction != other.current_forecasted_wind_direction) return false
        if (!forecast_24h_temperature.contentEquals(other.forecast_24h_temperature)) return false
        if (!forecast_24h_humidity.contentEquals(other.forecast_24h_humidity)) return false
        if (!forecast_24h_dewpoint.contentEquals(other.forecast_24h_dewpoint)) return false
        if (!forecast_24h_precipitation_total.contentEquals(other.forecast_24h_precipitation_total)) return false
        if (!forecast_24h_precipitation_probability.contentEquals(other.forecast_24h_precipitation_probability)) return false
        if (!forecast_24h_wind_speed.contentEquals(other.forecast_24h_wind_speed)) return false
        if (!forecast_24h_wind_gusts.contentEquals(other.forecast_24h_wind_gusts)) return false
        if (!forecast_24h_wind_direction.contentEquals(other.forecast_24h_wind_direction)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = station_id.hashCode()
        result = 31 * result + current_temperature.hashCode()
        result = 31 * result + current_humidity.hashCode()
        result = 31 * result + current_dewpoint.hashCode()
        result = 31 * result + current_precipitation.hashCode()
        result = 31 * result + current_timestamp.hashCode()
        result = 31 * result + forecast_made.hashCode()
        result = 31 * result + current_forecasted_precipitation_probability.hashCode()
        result = 31 * result + current_forecasted_wind_speed.hashCode()
        result = 31 * result + current_forecasted_wind_gusts.hashCode()
        result = 31 * result + current_forecasted_wind_direction.hashCode()
        result = 31 * result + forecast_24h_temperature.contentHashCode()
        result = 31 * result + forecast_24h_humidity.contentHashCode()
        result = 31 * result + forecast_24h_dewpoint.contentHashCode()
        result = 31 * result + forecast_24h_precipitation_total.contentHashCode()
        result = 31 * result + forecast_24h_precipitation_probability.contentHashCode()
        result = 31 * result + forecast_24h_wind_speed.contentHashCode()
        result = 31 * result + forecast_24h_wind_gusts.contentHashCode()
        result = 31 * result + forecast_24h_wind_direction.contentHashCode()
        return result
    }
}