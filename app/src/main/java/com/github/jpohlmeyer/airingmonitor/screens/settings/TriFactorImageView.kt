package com.github.jpohlmeyer.airingmonitor.screens.settings

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.doOnLayout
import kotlin.math.pow
import kotlin.math.sqrt


class TriFactorImageView(context: Context, attributeSet: AttributeSet) :
    AppCompatImageView(context, attributeSet) {
    private var dot: PointF? = null
    private val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var sideLength: Double = 0.0
    private var safetyPos = Pair(0.0, 0.0)
    private var energyComfortPos = Pair(0.0, 0.0)
    private var healthPos = Pair(0.0, 0.0)


    init {
        this.doOnLayout {
            val effectiveHeight = this.height.toDouble() - 5
            this.sideLength =
                this.length(effectiveHeight, (effectiveHeight / 2.0))
            this.safetyPos = Pair(this.width / 2.0, 5.0)
            this.energyComfortPos = Pair((this.width - this.sideLength) / 2.0, effectiveHeight)
            this.healthPos = Pair((this.width + this.sideLength) / 2.0, effectiveHeight)
        }
        setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                drawClippedDot(event.x.toDouble(), event.y.toDouble())
            }
            this.performClick()
        }
    }

    fun drawClippedDot(posX: Double, posY: Double) {
        val clippedPos = this.clipPosInTriangle(posX, posY)
        this.drawDot(PointF(clippedPos.first.toFloat(), clippedPos.second.toFloat()))
    }

    private fun clipPosInTriangle(xPos: Double, yPos: Double): Pair<Double, Double> {
        return if (xPos < 0 || yPos < 0) {
            Pair((this.healthPos.first - this.energyComfortPos.first)/2 + this.energyComfortPos.first, (this.energyComfortPos.second - this.safetyPos.second) / 2 + this.safetyPos.second)
        } else {
            if (yPos >= this.energyComfortPos.second) {
                if (xPos <= this.energyComfortPos.first) {
                    Pair(this.energyComfortPos.first, this.energyComfortPos.second)
                } else if (xPos >= this.healthPos.first) {
                    Pair(this.healthPos.first, this.healthPos.second)
                } else {
                    Pair(xPos, this.energyComfortPos.second)
                }
            } else if (yPos <= this.safetyPos.second) {
                Pair(safetyPos.first, safetyPos.second)
            } else if (xPos <= this.safetyPos.first) {
                val m =
                    (this.energyComfortPos.second - this.safetyPos.second) / (this.safetyPos.first - this.energyComfortPos.first)
                if (yPos < m * (this.safetyPos.first - xPos)) {
                    Pair(-yPos / m + this.safetyPos.first, yPos)
                } else {
                    Pair(xPos, yPos)
                }
            } else {
                val m =
                    (this.healthPos.second - this.safetyPos.second) / (this.healthPos.first - this.safetyPos.first)
                if (yPos < m * (xPos - this.safetyPos.first)) {
                    Pair(yPos / m + this.safetyPos.first, yPos)
                } else {
                    Pair(xPos, yPos)
                }
            }
        }
    }

    private fun length(x: Double, y: Double): Double {
        return sqrt(x.pow(2) + y.pow(2))
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        this.paint.color = Color.RED
        this.paint.style = Paint.Style.FILL_AND_STROKE
        this.dot?.apply {
            canvas.drawCircle(this.x, this.y, 10F, paint)
        }
    }

    fun drawDot(dotPoint: PointF) {
        this.dot = dotPoint
        this.invalidate()
    }

    fun getFactors(): List<Double> {
        var safety = 1 / 3.0
        var energyComfort = 1 / 3.0
        var health = 1 / 3.0
        var dotPosX = -1.0
        var dotPosY = -1.0
        if (dot != null) {
            val localDot = this.dot!!

            dotPosX = localDot.x.toDouble()
            dotPosY = localDot.y.toDouble()

            // line from safety corner (A) through dot(D) onto side (M)
            // safety = lenght(MD)/length(MA)
            if (localDot.x.toDouble() == safetyPos.first) {
                if (localDot.y.toDouble() == safetyPos.second) {
                    safety = 1.0
                } else {
                    val otherSideToDot = energyComfortPos.second - localDot.y.toDouble()
                    val safetyToOtherSide = energyComfortPos.second - safetyPos.second
                    safety = otherSideToDot / safetyToOtherSide
                }
            } else {
                val slope = if (localDot.x > safetyPos.first) {
                    (localDot.y - safetyPos.second) / (localDot.x - safetyPos.first)
                } else {
                    (safetyPos.second - localDot.y) / (safetyPos.first - localDot.x)
                }
                val intersectionX =
                    safetyPos.first + ((energyComfortPos.second - safetyPos.second) / slope)
                val intersectionY = energyComfortPos.second
                val otherSideToDot =
                    this.length(intersectionX - localDot.x, intersectionY - localDot.y)
                val safetyToOtherSide =
                    this.length(intersectionX - safetyPos.first, intersectionY - safetyPos.second)
                safety = otherSideToDot / safetyToOtherSide
            }

            // line from energyComfort corner (B) through dot(D) onto side (N)
            // energyComfort = lenght(ND)/length(NB)
            if (localDot.y.toDouble() >= energyComfortPos.second) {
                if (localDot.x.toDouble() <= energyComfortPos.first) {
                    energyComfort = 1.0
                } else {
                    val otherSideToDot = if (localDot.x.toDouble() >= healthPos.first) {
                        0.0
                    } else {
                        healthPos.first - localDot.x.toDouble()
                    }
                    val energyToOtherSide = healthPos.first - energyComfortPos.first
                    energyComfort = otherSideToDot / energyToOtherSide
                }
            } else {
                val slopeED = (localDot.y - energyComfortPos.second) / (localDot.x - energyComfortPos.first)
                val yOffsetED = energyComfortPos.second - slopeED * energyComfortPos.first
                val slopeCH =
                    (healthPos.second - safetyPos.second) / (healthPos.first - safetyPos.first)
                val yOffsetCH = safetyPos.second - slopeCH * safetyPos.first

                val intersectionX = (yOffsetCH - yOffsetED) / (slopeED - slopeCH)
                val intersectionY = yOffsetED + slopeED * intersectionX
                val otherSideToDot =
                    this.length(intersectionX - localDot.x, intersectionY - localDot.y)
                val energyToOtherSide =
                    this.length(intersectionX - energyComfortPos.first, intersectionY - energyComfortPos.second)
                energyComfort = otherSideToDot / energyToOtherSide
            }

            // line from health corner (C) through dot(D) onto side (L)
            // health = lenght(LD)/length(LC)
            if (localDot.y.toDouble() >= healthPos.second) {
                if (localDot.x.toDouble() >= healthPos.first) {
                    health = 1.0
                } else {
                    val otherSideToDot = if (localDot.x.toDouble() <= energyComfortPos.first) {
                        0.0
                    } else {
                        localDot.x.toDouble() - energyComfortPos.first
                    }
                    val healthToOtherSide = healthPos.first - energyComfortPos.first
                    health = otherSideToDot / healthToOtherSide
                }
            } else {
                val slopeHD = (healthPos.second - localDot.y) / (healthPos.first - localDot.x)
                val yOffsetHD = healthPos.second - slopeHD * healthPos.first
                val slopeEC =
                    (safetyPos.second - energyComfortPos.second) / (safetyPos.first - energyComfortPos.first)
                val yOffsetEC = energyComfortPos.second - slopeEC * energyComfortPos.first

                val intersectionX = (yOffsetEC - yOffsetHD) / (slopeHD - slopeEC)
                val intersectionY = yOffsetHD + slopeHD * intersectionX
                val otherSideToDot =
                    this.length(intersectionX - localDot.x, intersectionY - localDot.y)
                val healthToOtherSide =
                    this.length(intersectionX - healthPos.first, intersectionY - healthPos.second)
                health = otherSideToDot / healthToOtherSide
            }
        }
        return listOf(safety, energyComfort, health, dotPosX, dotPosY)
    }
}