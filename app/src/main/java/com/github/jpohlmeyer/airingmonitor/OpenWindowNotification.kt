package com.github.jpohlmeyer.airingmonitor

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Observer
import com.github.jpohlmeyer.airingmonitor.data.model.CombiListData
import com.github.jpohlmeyer.airingmonitor.data.model.WindowState
import com.github.jpohlmeyer.airingmonitor.data.repository.CombiDataRepository
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.abs

@Singleton
class OpenWindowNotification @Inject constructor(
    combiDataRepository: CombiDataRepository,
    @ApplicationContext private val context: Context
) {

    private val dataObserver: Observer<CombiListData>

    private var lastNotificationTimestamp: Long? = null


    init {
        dataObserver = Observer<CombiListData> { newData ->
            observerUpdate(newData)
        }
        combiDataRepository.combiLiveData.observeForever(dataObserver)
    }

    private fun observerUpdate(combiListData: CombiListData) {
        val time = System.currentTimeMillis() / 1000
        Log.w(TAG, "observerUpdate time:$time lastNotification:$lastNotificationTimestamp")
        if (lastNotificationTimestamp == null || time - lastNotificationTimestamp!! > 60 * 5) {
            if (combiListData.airingForecastData != null && combiListData.airingForecastData!!.windowInfo.windowstate != WindowState.CLOSED && combiListData.airingForecastData!!.windowInfo.notify) {
                val latestSensordata = combiListData.sensorData.last()
                if (abs(latestSensordata.t_indoor_scd - latestSensordata.t_outdoor_sht) > 5) {
                    sendNotification(time.toInt())
                    lastNotificationTimestamp = time
                }
            }


        }
    }

    private fun sendNotification(id: Int) {
        val builder = NotificationCompat.Builder(context, Constants.CHANNEL_ID_ALERT)
            .setSmallIcon(R.drawable.ic_baseline_thermostat_24)
            .setContentTitle("Airing Alarm")
            .setContentText("Window detected open for an extended time. Please ensure you have not forgotten to close it.")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
        with(NotificationManagerCompat.from(context)) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.POST_NOTIFICATIONS
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                notify(id, builder.build())
            } else {
                Log.w(TAG, "Tried sending notification, but permission is not granted.")
            }
        }
    }

    companion object {
        private const val TAG = "OpenWindowNotification"
    }
}