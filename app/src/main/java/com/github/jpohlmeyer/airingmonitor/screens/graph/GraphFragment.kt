package com.github.jpohlmeyer.airingmonitor.screens.graph

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.github.jpohlmeyer.airingmonitor.data.model.CombiListData
import com.github.jpohlmeyer.airingmonitor.data.model.SensorData
import com.github.jpohlmeyer.airingmonitor.data.model.WindowData
import com.github.jpohlmeyer.airingmonitor.data.model.WindowState
import com.github.jpohlmeyer.airingmonitor.databinding.FragmentGraphBinding
import com.github.jpohlmeyer.airingmonitor.screens.AppViewModel
import com.github.jpohlmeyer.airingmonitor.screens.graph.axisformatter.NoLabelAxisValueFormatter
import com.github.jpohlmeyer.airingmonitor.screens.graph.axisformatter.UnitAxisValueFormatter
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import kotlin.math.abs
import kotlin.reflect.KClass

@AndroidEntryPoint
abstract class GraphFragment : Fragment() {

    private var _binding: FragmentGraphBinding? = null
    protected val binding get() = _binding!!

    protected val viewModel: AppViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGraphBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initChart()

        val dataObserver = Observer<CombiListData> { newData ->
            updateChartData(newData.sensorData)
            updateWindowData(newData.windowData, newData.sensorData)
        }
        viewModel.combiLiveData.observe(viewLifecycleOwner, dataObserver)
    }

    abstract fun updateChartData(newSensorData: List<SensorData>)

    abstract fun initChart()

    protected fun initChart(unit: String) {
        val fontScale = resources.configuration.fontScale
        binding.chart.extraTopOffset = 10f
        binding.chart.xAxis.textSize = 15f * fontScale
        binding.chart.axisLeft.textSize = 15f * fontScale
        binding.chart.axisRight.textSize = 15f * fontScale
        binding.chart.legend.textSize = 15f * fontScale
        binding.chart.description.isEnabled = false
        binding.chart.xAxis.setDrawLabels(true)
        binding.chart.xAxis.setDrawLimitLinesBehindData(true)
        binding.chart.xAxis.position = XAxis.XAxisPosition.TOP
        binding.chart.axisLeft.valueFormatter = UnitAxisValueFormatter(unit)
        binding.chart.axisRight.valueFormatter = NoLabelAxisValueFormatter()
    }

    protected fun redrawLineData(lineData: LineData, timestamps: List<Long>) {
        val labels = mutableListOf<String>()
        for (timestamp in timestamps) {
            val datetime = Instant.ofEpochSecond(timestamp).atZone(ZoneId.systemDefault())
                .toLocalDateTime()
            labels.add(DateTimeFormatter.ofPattern("HH:mm").format(datetime))
        }

        binding.chart.data = lineData
        binding.chart.xAxis.valueFormatter = IndexAxisValueFormatter(labels)
        binding.chart.invalidate()
    }

    private fun updateWindowData(
        newWindowData: List<WindowData>,
        matchingSensorData: List<SensorData>
    ) {
        if (newWindowData.isEmpty()) {
            Log.w(LOG_TAG, "Tried to update chart with empty windowdata list")
            return
        }
        binding.chart.xAxis.removeAllLimitLines()

        val limitLines = mutableListOf<LimitLine>()

        var currentWindowState = newWindowData[0].windowstate

        limitLines.add(createLimitLine(0, currentWindowState))

        val windowChanges = mutableListOf<WindowData>()
        for (data in newWindowData) {
            if (data.windowstate != currentWindowState) {
                windowChanges.add(data)
                currentWindowState = data.windowstate
            }
        }

        val initialSensorId = matchingSensorData[0].id
        var windowChangesCounter = 0
        for (sensorData in matchingSensorData) {
            if (windowChangesCounter >= windowChanges.size) {
                break;
            }
            if (windowChanges[windowChangesCounter].timestamp + 31 < sensorData.timestamp) {
                windowChangesCounter++
            }
            if (abs(sensorData.timestamp - windowChanges[windowChangesCounter].timestamp) <= 30) {
                limitLines.add(
                    createLimitLine(
                        sensorData.id - initialSensorId,
                        windowChanges[windowChangesCounter].windowstate
                    )
                )
                windowChangesCounter++;
            }
        }

        binding.chart.xAxis.limitLines.addAll(limitLines)
        binding.chart.invalidate()
    }

    private fun createLimitLine(value: Int, windowstate: WindowState): LimitLine {
        val limitLine = LimitLine(value.toFloat(), "")
        limitLine.lineColor = when (windowstate) {
            WindowState.OPEN -> Color.rgb(255, 150, 150)
            WindowState.TILTED -> Color.rgb(255, 255, 50)
            else -> Color.rgb(150, 255, 150)
        }
        limitLine.lineWidth = 2f
        return limitLine
    }

    protected fun createDataSet(title: String, color: Int, lineData: List<Entry>): LineDataSet {
        val dataSet = LineDataSet(lineData, title)
        dataSet.color = color
        dataSet.setDrawCircles(false)
        dataSet.axisDependency = YAxis.AxisDependency.LEFT
        dataSet.lineWidth = 2f
        return dataSet
    }

    companion object {
        private const val LOG_TAG = "GraphFragment"
    }
}

enum class GraphFragmentType(
    val stringValue: String,
    val graphFragment: KClass<out GraphFragment>
) {
    TEMPERATURE("Temperature", TempGraphFragment::class),
    HUMIDITY("Humidity", HumGraphFragment::class),
    CO2("CO2", CO2GraphFragment::class)
}

@AndroidEntryPoint
class TempGraphFragment : GraphFragment() {

    override fun initChart() {
        initChart("°C")
        binding.chart.setMaxVisibleValueCount(50)
    }

    override fun updateChartData(newSensorData: List<SensorData>) {
        if (newSensorData.isEmpty()) {
            Log.w(LOG_TAG, "Tried to update chart with empty sensordata list")
            return
        }

        Log.d(LOG_TAG, "Update Temperature chart")

        val tempLineData = LineData()
        val t_indoor_scd = mutableListOf<Entry>()
        val dewpoint_indoor = mutableListOf<Entry>()
        val t_outdoor_sht = mutableListOf<Entry>()
        val dewpoint_outdoor = mutableListOf<Entry>()
        val timestamps = mutableListOf<Long>()
        val idStart = newSensorData[0].id
        for (value in newSensorData) {
            t_indoor_scd.add(Entry(value.id.toFloat() - idStart, value.t_indoor_scd))
            dewpoint_indoor.add(Entry(value.id.toFloat() - idStart, value.dewpoint_indoor))
            t_outdoor_sht.add(Entry(value.id.toFloat() - idStart, value.t_outdoor_sht))
            dewpoint_outdoor.add(Entry(value.id.toFloat() - idStart, value.dewpoint_outdoor))
            timestamps.add(value.timestamp)
        }

        tempLineData.addDataSet(
            createDataSet(
                "In",
                Color.rgb(255, 0, 0),
                t_indoor_scd
            )
        )
        tempLineData.addDataSet(
            createDataSet(
                "Dew (In)",
                Color.rgb(255, 200, 200),
                dewpoint_indoor
            )
        )
        tempLineData.addDataSet(
            createDataSet(
                "Out",
                Color.rgb(0, 255, 0),
                t_outdoor_sht
            )
        )
        tempLineData.addDataSet(
            createDataSet(
                "Dew (Out)",
                Color.rgb(200, 255, 200),
                dewpoint_outdoor
            )
        )
        redrawLineData(tempLineData, timestamps)

        Log.d(LOG_TAG, "Update Temperature chart done")
    }

    companion object {
        private const val LOG_TAG = "TempGraphFragment"
    }
}

@AndroidEntryPoint
class HumGraphFragment : GraphFragment() {

    override fun initChart() {
        initChart("%")
        binding.chart.setMaxVisibleValueCount(20)
    }

    override fun updateChartData(newSensorData: List<SensorData>) {
        if (newSensorData.isEmpty()) {
            Log.w(LOG_TAG, "Tried to update chart with empty sensordata list")
            return
        }

        Log.d(LOG_TAG, "Update Humidity chart")

        val humLineData = LineData()
        val h_indoor_scd = mutableListOf<Entry>()
        val h_outdoor_sht = mutableListOf<Entry>()
        val timestamps = mutableListOf<Long>()
        val idStart = newSensorData[0].id
        for (value in newSensorData) {
            h_indoor_scd.add(Entry(value.id.toFloat() - idStart, value.h_indoor_scd))
            h_outdoor_sht.add(Entry(value.id.toFloat() - idStart, value.h_outdoor_sht))
            timestamps.add(value.timestamp)
        }
        humLineData.addDataSet(createDataSet("Indoor", Color.rgb(0, 0, 255), h_indoor_scd))
        humLineData.addDataSet(createDataSet("Outdoor", Color.rgb(0, 255, 0), h_outdoor_sht))
        redrawLineData(humLineData, timestamps)

        Log.d(LOG_TAG, "Update Humidity chart done")
    }

    companion object {
        private const val LOG_TAG = "HumGraphFragment"
    }
}

@AndroidEntryPoint
class CO2GraphFragment : GraphFragment() {

    override fun initChart() {
        initChart("ppm")
        binding.chart.setMaxVisibleValueCount(10)
    }

    override fun updateChartData(newSensorData: List<SensorData>) {
        if (newSensorData.isEmpty()) {
            Log.w(LOG_TAG, "Tried to update chart with empty sensordata list")
            return
        }
        Log.d(LOG_TAG, "Update CO2 chart")

        val co2LineData = LineData()
        val co2Data = mutableListOf<Entry>()
        val timestamps = mutableListOf<Long>()
        val idStart = newSensorData[0].id
        for (value in newSensorData) {
            co2Data.add(Entry(value.id.toFloat() - idStart, value.co2))
            timestamps.add(value.timestamp)
        }
        co2LineData.addDataSet(createDataSet("CO2", Color.GRAY, co2Data))
        redrawLineData(co2LineData, timestamps)

        Log.d(LOG_TAG, "Update CO2 chart done")
    }

    companion object {
        private const val LOG_TAG = "CO2GraphFragment"
    }
}