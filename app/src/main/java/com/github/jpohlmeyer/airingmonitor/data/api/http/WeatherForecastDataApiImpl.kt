package com.github.jpohlmeyer.airingmonitor.data.api.http

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.api.ApiException
import com.github.jpohlmeyer.airingmonitor.data.api.WeatherForecastDataApi
import com.github.jpohlmeyer.airingmonitor.data.model.WeatherForecastData
import com.google.gson.Gson
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import javax.inject.Singleton

class WeatherForecastDataApiImpl @Inject constructor(
    private val volleyHTTPClient: VolleyHTTPClient,
    @ApplicationContext applicationContext: Context
) : WeatherForecastDataApi {

    private val gson = Gson()

    private val sharedPreferences: SharedPreferences
    private val settingsHubAdress: String
    private val defaultBaseUrl: String

    init {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        settingsHubAdress = applicationContext.resources.getString(R.string.settings_hub_address)
        defaultBaseUrl = applicationContext.resources.getString(R.string.default_hub_host)
    }

    private suspend fun updateSingleLatestData() : WeatherForecastData {
        return suspendCancellableCoroutine { continuation ->
            val baseUrl = sharedPreferences.getString(settingsHubAdress, defaultBaseUrl)!!
            val url = "http://$baseUrl/weather_forecast"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                { response: String ->
                    val weatherForecastData: WeatherForecastData = gson.fromJson(response, WeatherForecastData::class.java)
                    continuation.resumeWith(Result.success(weatherForecastData))
                },
                { error ->
                    val msg = if (error.networkResponse == null) {
                        "Data request failed: Network response is null"
                    } else {
                        "Data request failed: " + error.message
                    }
                    continuation.cancel(ApiException(msg))
                })
            volleyHTTPClient.addRequestToQueue(stringRequest)
        }
    }

    override suspend fun fetchSingleLatestWeatherForecastData(): WeatherForecastData {
        return updateSingleLatestData()
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class WeatherForecastApiModule {
    @Binds
    abstract fun weatherForecastData(weatherForecastDataApiImpl: WeatherForecastDataApiImpl) : WeatherForecastDataApi
}