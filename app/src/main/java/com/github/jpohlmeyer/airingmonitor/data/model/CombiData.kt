package com.github.jpohlmeyer.airingmonitor.data.model

class CombiData(
    var sensorData: SensorData?,
    var windowData: WindowData?,
    var airingForecast: AiringForecastContainer? = null
)