package com.github.jpohlmeyer.airingmonitor.screens.graph

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.github.jpohlmeyer.airingmonitor.databinding.FragmentTabsBinding
import com.github.jpohlmeyer.airingmonitor.screens.AppViewModel
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TabsFragment : Fragment() {

    private var _binding: FragmentTabsBinding? = null
    private val binding get() = _binding!!

    protected val viewModel: AppViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTabsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.viewpager.isUserInputEnabled = false
        binding.viewpager.adapter = TabsAdapter(this)
        TabLayoutMediator(binding.tabs, binding.viewpager) { tab, position ->
            tab.text = GraphFragmentType.values()[position].stringValue
        }.attach()
    }

    override fun onResume() {
        viewModel.updateData()
        super.onResume()
    }


}