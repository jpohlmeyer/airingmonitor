package com.github.jpohlmeyer.airingmonitor.screens.graph.axisformatter

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter

class UnitAxisValueFormatter(private val unit: String): IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return "${value.toInt()} $unit"
    }
}