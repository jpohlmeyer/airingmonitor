package com.github.jpohlmeyer.airingmonitor.data.api

import com.github.jpohlmeyer.airingmonitor.data.model.WeatherForecastData

interface WeatherForecastDataApi {
    suspend fun fetchSingleLatestWeatherForecastData(): WeatherForecastData?
}