package com.github.jpohlmeyer.airingmonitor.data.repository

import android.util.Log
import android.view.Window
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.jpohlmeyer.airingmonitor.data.api.*
import com.github.jpohlmeyer.airingmonitor.data.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherForecastDataRepository @Inject constructor(
    private val weatherForecastDataApi: WeatherForecastDataApi
) {

    private val _weatherForecastLiveData: MutableLiveData<WeatherForecastData> = MutableLiveData()
    val weatherForecastLiveData: LiveData<WeatherForecastData>
        get() = _weatherForecastLiveData

    private var weatherForecastData: WeatherForecastData? = null

    suspend fun fetchLatestWeatherForecastData(): WeatherForecastData? {
        return withContext(Dispatchers.IO) {
            try {
                weatherForecastData = weatherForecastDataApi.fetchSingleLatestWeatherForecastData()
            } catch (apiException: ApiException) {
                Log.w(LOG_TAG, "Refresh of weather forecast failed with Exception:")
                Log.w(LOG_TAG, apiException)
            }

            if (weatherForecastData != null) {
                _weatherForecastLiveData.postValue(weatherForecastData!!)
            }
            return@withContext weatherForecastData
        }
    }

    companion object {
        private const val LOG_TAG = "WeatherForecastDataRepository"
    }
}