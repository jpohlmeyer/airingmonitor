package com.github.jpohlmeyer.airingmonitor.data.model

import com.google.gson.annotations.SerializedName

enum class WindowState {
    @SerializedName("OPEN")
    OPEN,
    @SerializedName("TILTED")
    TILTED,
    @SerializedName("CLOSED")
    CLOSED
}