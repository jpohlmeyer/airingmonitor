package com.github.jpohlmeyer.airingmonitor.data.model

data class SensorData(
    val id: Int,
    val timestamp: Long,
    val occupied: Boolean,
    val h_indoor_scd: Float,
    val t_indoor_scd: Float,
    val co2: Float,
    val dewpoint_indoor: Float,
    val h_outdoor_sht: Float,
    val t_outdoor_sht: Float,
    val dewpoint_outdoor: Float
)
