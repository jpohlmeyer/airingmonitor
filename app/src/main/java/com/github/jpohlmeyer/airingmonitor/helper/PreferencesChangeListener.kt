package com.github.jpohlmeyer.airingmonitor.helper

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.util.Log
import androidx.preference.PreferenceManager
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.api.http.VolleyHTTPClient
import dagger.hilt.android.qualifiers.ApplicationContext
import org.json.JSONObject
import java.nio.charset.Charset
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesChangeListener @Inject constructor(
    private val volleyHTTPClient: VolleyHTTPClient,
    @ApplicationContext private val context: Context
) : OnSharedPreferenceChangeListener {

    private val defaultBaseUrl: String
    private val settingsHubAdress: String

    init {
        PreferenceManager.getDefaultSharedPreferences(context)
            .registerOnSharedPreferenceChangeListener(this)
        settingsHubAdress = context.resources.getString(R.string.settings_hub_address)
        defaultBaseUrl = context.resources.getString(R.string.default_hub_host)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        val airingFactorsWeightsKey = context.resources.getString(R.string.settings_airing_factor_weights)
        val weatherStationKey = context.resources.getString(R.string.settings_weatherstation)
        if (key.equals(airingFactorsWeightsKey) ||
            key.equals(weatherStationKey)) {
            val triFactorSetting = sharedPreferences?.getString(airingFactorsWeightsKey, null)
            val weatherStationSetting = sharedPreferences?.getString(weatherStationKey, null)
            if (triFactorSetting != null && weatherStationSetting != null) {
                val triFactorSettingArray = triFactorSetting.split(";")
                val baseUrl = sharedPreferences.getString(settingsHubAdress, defaultBaseUrl)!!
                val url = "http://$baseUrl/app_preferences"
                val requestBody = JSONObject(
                    mapOf<String, Any>(
                        Pair("safety", triFactorSettingArray[0].toDouble()),
                        Pair("energycomfort", triFactorSettingArray[1].toDouble()),
                        Pair("health", triFactorSettingArray[2].toDouble()),
                        Pair("weatherstation", weatherStationSetting)
                    )
                ).toString()
                val stringRequest: StringRequest =
                    object : StringRequest(Method.POST, url,
                        Response.Listener {
                            Log.i(LOG_TAG, "Saving settings to server successful: $requestBody")
                        },
                        Response.ErrorListener {
                            Log.e(LOG_TAG, "Error saving settings change to server: $requestBody")
                        }
                    ) {
                        override fun getBodyContentType(): String {
                            return "application/json; charset=utf-8"
                        }

                        override fun getBody(): ByteArray {
                            return requestBody.toByteArray(Charset.forName("utf-8"))
                        }
                    }
                volleyHTTPClient.addRequestToQueue(stringRequest)
            }
        }
    }

    companion object {
        private val LOG_TAG = "TriFactorChangeListener"
    }
}