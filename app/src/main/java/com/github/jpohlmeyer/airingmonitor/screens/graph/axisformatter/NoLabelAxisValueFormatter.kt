package com.github.jpohlmeyer.airingmonitor.screens.graph.axisformatter

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter

class NoLabelAxisValueFormatter: IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return ""
    }
}