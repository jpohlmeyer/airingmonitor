package com.github.jpohlmeyer.airingmonitor.data.api

import com.github.jpohlmeyer.airingmonitor.data.model.SensorData

interface SensorDataApi {
    suspend fun fetchLatestSensorData(): List<SensorData>
    suspend fun fetchSensorDataFromTo(from: Long,to: Long): List<SensorData>
    suspend fun fetchSingleLatestSensorData(): SensorData
}