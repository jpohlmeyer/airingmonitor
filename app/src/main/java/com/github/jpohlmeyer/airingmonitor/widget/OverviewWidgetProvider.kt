package com.github.jpohlmeyer.airingmonitor.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import androidx.work.WorkManager
import com.github.jpohlmeyer.airingmonitor.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


/**
 * Implementation of App Widget functionality.
 */
@AndroidEntryPoint
class OverviewWidgetProvider : AppWidgetProvider() {

    @Inject
    lateinit var widgetUpdater: WidgetUpdater

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        widgetUpdater.initWidgets(appWidgetIds)
        super.onUpdate(context, appWidgetManager, appWidgetIds)
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
        widgetUpdater.startObserving()
        super.onEnabled(context)
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
        widgetUpdater.stopObserving()
        super.onDisabled(context)
    }

    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        for (appWidgetId in appWidgetIds) {
            val uniqueName = Constants.WIDGET_PERIODIC_WORKER_TAG +appWidgetId
            WorkManager
                .getInstance(context).cancelUniqueWork(uniqueName)
        }
        super.onDeleted(context, appWidgetIds)
    }
}