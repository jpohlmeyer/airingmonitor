package com.github.jpohlmeyer.airingmonitor.data.api.http

import com.github.jpohlmeyer.airingmonitor.data.model.WindowInfoData
import com.google.gson.JsonArray
import com.google.gson.JsonElement

data class AiringForecastJsonIntermediate(
    val airingForecast: JsonElement,
    val windowInfo: WindowInfoData
)