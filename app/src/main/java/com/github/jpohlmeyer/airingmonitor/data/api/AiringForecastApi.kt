package com.github.jpohlmeyer.airingmonitor.data.api

import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastContainer

interface AiringForecastApi {
    suspend fun fetchAiringForecast(): AiringForecastContainer
}