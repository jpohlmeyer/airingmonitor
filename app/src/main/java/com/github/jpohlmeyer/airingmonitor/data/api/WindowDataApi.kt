package com.github.jpohlmeyer.airingmonitor.data.api

import com.github.jpohlmeyer.airingmonitor.data.model.WindowData

interface WindowDataApi {
    suspend fun fetchLatestWindowData(): List<WindowData>
    suspend fun fetchWindowDataFromTo(from: Long,to: Long): List<WindowData>
    suspend fun fetchSingleLatestWindowData(): WindowData
}