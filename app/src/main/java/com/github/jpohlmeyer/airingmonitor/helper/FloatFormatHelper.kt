package com.github.jpohlmeyer.airingmonitor.helper

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FloatFormatHelper @Inject constructor(){

    fun formatFloat(value: Float, decimalDigits: Int): String {
        val df = DecimalFormat()
        df.decimalFormatSymbols = DecimalFormatSymbols(Locale.US)
        df.maximumFractionDigits = decimalDigits
        return df.format(value)
    }
}