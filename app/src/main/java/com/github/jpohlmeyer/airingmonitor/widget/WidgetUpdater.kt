package com.github.jpohlmeyer.airingmonitor.widget

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.widget.RemoteViews
import androidx.lifecycle.Observer
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.model.*
import com.github.jpohlmeyer.airingmonitor.data.repository.CombiDataRepository
import com.github.jpohlmeyer.airingmonitor.helper.AiringInfoHelper
import com.github.jpohlmeyer.airingmonitor.helper.FloatFormatHelper
import com.github.jpohlmeyer.airingmonitor.screens.MainActivity
import dagger.hilt.android.qualifiers.ApplicationContext
import java.time.Instant
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WidgetUpdater @Inject constructor(
    private val combiDataRepository: CombiDataRepository,
    @ApplicationContext private val context: Context
) {

    private val dataObserver: Observer<CombiListData>

    @Inject
    lateinit var floatFormatHelper: FloatFormatHelper

    @Inject
    lateinit var airingInfoHelper: AiringInfoHelper


    init {
        dataObserver = Observer<CombiListData> { newData ->
            observerUpdate(newData)
        }
        combiDataRepository.combiLiveData.observeForever(dataObserver)
    }

    fun startObserving() {
        Log.d(LOG_TAG, "Start observing data")
        combiDataRepository.combiLiveData.observeForever(dataObserver)
    }

    fun stopObserving() {
        Log.d(LOG_TAG, "Stop observing data")
        combiDataRepository.combiLiveData.removeObserver(dataObserver)
    }

    private fun observerUpdate(combiListData: CombiListData) {
        Log.d(LOG_TAG, "Starting observer widget update")
        var lastSensorData: SensorData? = null
        if (!combiListData.sensorData.isEmpty()) {
            lastSensorData = combiListData.sensorData.last()
        }
        var lastWindowData: WindowData? = null
        if (!combiListData.windowData.isEmpty()) {
            lastWindowData = combiListData.windowData.last()
        }
        var airingForecastData: AiringForecastContainer? = combiListData.airingForecastData
        val combiData = CombiData(lastSensorData, lastWindowData, airingForecastData)
        val appWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(
            ComponentName(
                context.packageName,
                OverviewWidgetProvider::class.java.name
            )
        )
        updateWidgets(appWidgetIds, combiData)
    }

    private fun updateWidgets(appWidgetIds: IntArray, combiData: CombiData) {
        val views = RemoteViews(context.packageName, R.layout.overview_widget)

        var ventilationCurrentText = "-"
        var ventilationCurrentColor = Color.GRAY
        var ventilationNextOptimumText = "-"
        var ventilationNextOptimumColor = Color.GRAY
        combiData.airingForecast?.let {
            ventilationCurrentText = airingInfoHelper.calculateAdviceText(it)
            ventilationCurrentColor = airingInfoHelper.calculateColor(it)
            val nextOptimumIndex = airingInfoHelper.calculateOptimumPosition(it)
            if (nextOptimumIndex < 1) {
                ventilationNextOptimumText = "No optimum found."
                ventilationNextOptimumColor = Color.GRAY
            } else {
                ventilationNextOptimumText = if (nextOptimumIndex == 1) {
                    "Next optimum: now"
                } else {
                    val datetime =
                        Instant.ofEpochSecond(it.airingForecast[nextOptimumIndex].timestamp)
                            .atZone(ZoneId.systemDefault()).toLocalDateTime();
                    val optimalTime = DateTimeFormatter.ofPattern("HH:mm").format(datetime)
                    "Next optimum: $optimalTime"
                }
                ventilationNextOptimumColor = airingInfoHelper.calculateColor(it, nextOptimumIndex)
            }
        }
        views.setTextViewText(R.id.start_ventilationAdvice, ventilationCurrentText)
        views.setTextViewText(R.id.start_next_optimum_value, ventilationNextOptimumText)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            views.setColorInt(
                R.id.start_ventilationAdvice,
                "setBackgroundColor",
                ventilationCurrentColor,
                ventilationCurrentColor
            )
            views.setColorInt(
                R.id.start_next_optimum_value,
                "setBackgroundColor",
                ventilationNextOptimumColor,
                ventilationNextOptimumColor
            )
        } else {
            views.setInt(
                R.id.start_ventilationAdvice,
                "setBackgroundColor",
                ventilationCurrentColor
            )
            views.setInt(
                R.id.start_next_optimum_value,
                "setBackgroundColor",
                ventilationNextOptimumColor
            )
        }

        var tIndoor = "-"
        var tOutdoor = "-"
        var hIndoor = "-"
        var hOutdoor = "-"
        var co2 = "-"
        if (combiData.sensorData != null) {
            tIndoor = floatFormatHelper.formatFloat(combiData.sensorData!!.t_indoor_scd, 1)
            tOutdoor = floatFormatHelper.formatFloat(combiData.sensorData!!.t_outdoor_sht, 1)
            hIndoor = floatFormatHelper.formatFloat(combiData.sensorData!!.h_indoor_scd, 1)
            hOutdoor = floatFormatHelper.formatFloat(combiData.sensorData!!.h_outdoor_sht, 1)
            co2 = floatFormatHelper.formatFloat(combiData.sensorData!!.co2, 0)
        }
        var windowData = "-"
        if (combiData.windowData != null) {
            windowData = combiData.windowData!!.windowstate.toString()
        }

        views.setTextViewText(R.id.start_indoor_temp, "$tIndoor/$tOutdoor°C")
        views.setTextViewText(R.id.start_indoor_hum, "$hIndoor/$hOutdoor%")
        views.setTextViewText(R.id.start_co2, "$co2 ppm")
        views.setTextViewText(R.id.start_window_state, windowData)
        var lastTimestamp = 0L
        if (combiData.sensorData != null && combiData.windowData == null) {
            lastTimestamp = combiData.sensorData!!.timestamp
        } else if (combiData.sensorData == null && combiData.windowData != null) {
            lastTimestamp = combiData.windowData!!.timestamp
        } else if (combiData.sensorData != null && combiData.windowData != null) {
            lastTimestamp = if (combiData.sensorData!!.timestamp < combiData.windowData!!.timestamp) {
                combiData.sensorData!!.timestamp
            } else {
                combiData.windowData!!.timestamp
            }
        }
        if (lastTimestamp > 0) {
            val datetime = Instant.ofEpochSecond(lastTimestamp).atZone(ZoneId.systemDefault())
                .toLocalDateTime()
            views.setTextViewText(
                R.id.widget_timestamp,
                "Last updated: " + DateTimeFormatter.ofPattern("HH:mm:ss").format(datetime)
            )
        }


        views.setOnClickPendingIntent(
            R.id.widget_layout,
            PendingIntent.getActivity(
                context,
                0,
                Intent(context, MainActivity::class.java),
                FLAG_IMMUTABLE
            )
        )

        AppWidgetManager.getInstance(context).updateAppWidget(appWidgetIds, views)
    }

    fun initWidgets(appWidgetIds: IntArray) {
        Log.d(LOG_TAG, "Init widget data")
        val combiData = CombiData(
            combiDataRepository.combiLiveData.value?.sensorData!!.last(),
            combiDataRepository.combiLiveData.value?.windowData!!.last(),
            combiDataRepository.combiLiveData.value?.airingForecastData
        )
        updateWidgets(appWidgetIds, combiData)
    }

    companion object {
        private const val LOG_TAG = "WidgetUpdater"
    }

}