package com.github.jpohlmeyer.airingmonitor.screens.forecast

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastData
import com.github.jpohlmeyer.airingmonitor.data.model.CombiData
import com.github.jpohlmeyer.airingmonitor.data.model.CombiListData
import com.github.jpohlmeyer.airingmonitor.data.persistence.ForecastAlarm
import com.github.jpohlmeyer.airingmonitor.databinding.FragmentForecastBinding
import com.github.jpohlmeyer.airingmonitor.screens.AppViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForecastFragment : Fragment() {

    private var _binding: FragmentForecastBinding? = null
    private val binding get() = _binding!!

    private val viewModel: AppViewModel by activityViewModels()

    private lateinit var forecastAdapter: ForecastAdapter
    private lateinit var weatherForecastView: RecyclerView
    private lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentForecastBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        weatherForecastView = binding.timelineRecyclerView
        layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        weatherForecastView.layoutManager = layoutManager
        val dataObserver = Observer<CombiListData> { newData ->
            newData.airingForecastData?.let {
                this.updateData(newData.airingForecastData!!.airingForecast)
            }
        }
        viewModel.combiLiveData.observe(viewLifecycleOwner, dataObserver)
        forecastAdapter = ForecastAdapter(requireContext(), viewModel)
        val reminderDataObserver = Observer<List<ForecastAlarm>> { newData ->
            forecastAdapter.updateReminderData(newData)
        }
        viewModel.reminderLiveData.observe(viewLifecycleOwner, reminderDataObserver)
        weatherForecastView.adapter = forecastAdapter
    }
//
//    override fun onResume() {
//        viewModel.updateForecastData()
//        super.onResume()
//    }

    private fun updateData(airingForecast: List<AiringForecastData>) {
        binding.updatingTextView.visibility = View.GONE
        if (airingForecast.isEmpty()) {
            weatherForecastView.visibility = View.GONE
            binding.emptyTextView.visibility = View.VISIBLE
        } else {
            weatherForecastView.visibility = View.VISIBLE
            binding.emptyTextView.visibility = View.GONE
        }
        forecastAdapter.updateData(airingForecast)
    }

    companion object {
        private const val LOG_TAG = "ForecastFragment"
    }

}