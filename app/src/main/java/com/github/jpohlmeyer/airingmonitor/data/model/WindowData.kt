package com.github.jpohlmeyer.airingmonitor.data.model

data class WindowData(
    val id: Int,
    val timestamp: Long,
    val windowstate: WindowState
)

