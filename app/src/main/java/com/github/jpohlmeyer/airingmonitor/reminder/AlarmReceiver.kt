package com.github.jpohlmeyer.airingmonitor.reminder

import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.github.jpohlmeyer.airingmonitor.Constants
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.persistence.ForecastAlarmDao
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@AndroidEntryPoint
class AlarmReceiver : BroadcastReceiver() {

    @Inject
    lateinit var forecastAlarmDao: ForecastAlarmDao

    @Inject
    lateinit var forecastReminderManager: ForecastReminderManager

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(LOG_TAG, "Received action")
        // Is triggered when alarm goes off, i.e. receiving a system broadcast
        if (intent.action == Constants.REMINDER_ACTION) {
            val timestamp = intent.getLongExtra(Constants.REMINDER_TIMESTAMP_KEY, -1)
            val datetime =
                Instant.ofEpochSecond(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
            val datetimeString = DateTimeFormatter.ofPattern("HH:mm").format(datetime)
            timestamp.toString()
            val builder = NotificationCompat.Builder(context, Constants.CHANNEL_ID_NOTICE)
                .setSmallIcon(R.drawable.baseline_alarm_24)
                .setContentTitle("Airing Reminder")
                .setContentText("You set an Airing Reminder for $datetimeString.")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
            with(NotificationManagerCompat.from(context)) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.POST_NOTIFICATIONS
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    notify(timestamp.toInt(), builder.build())
                } else {
                    Log.w(LOG_TAG, "Tried sending notification, but permission is not granted.")
                }
            }
            runBlocking {
                async {
                    forecastAlarmDao.deleteOld(timestamp)
                }
            }
            Log.d(LOG_TAG, "Alarm triggered + $timestamp")
        } else if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            Log.d(LOG_TAG, "Reschedule reminder on reboot.")
            runBlocking {
                async {
                    val timestamp = Instant.now().epochSecond
                    forecastAlarmDao.deleteOld(timestamp)
                    forecastReminderManager.rescheduleAlarms()
                }
            }
        }
    }

    companion object {
        private const val LOG_TAG = "AlarmReceiver"
    }
}