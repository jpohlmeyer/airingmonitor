package com.github.jpohlmeyer.airingmonitor.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.jpohlmeyer.airingmonitor.data.api.*
import com.github.jpohlmeyer.airingmonitor.data.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AiringForecastDataRepository @Inject constructor(
    private val airingForecastApi: AiringForecastApi
) {

    private val _airingForecastLiveData: MutableLiveData<AiringForecastContainer> = MutableLiveData()
    val airingForecastLiveData: LiveData<AiringForecastContainer>
        get() = _airingForecastLiveData

    private var airingForecast: AiringForecastContainer? = null

    suspend fun fetchAiringForecastData(): AiringForecastContainer? {
        return withContext(Dispatchers.IO) {
            try {
                airingForecast = airingForecastApi.fetchAiringForecast()
            } catch (apiException: ApiException) {
                Log.w(LOG_TAG, "Refresh of airing forecast failed with Exception:")
                Log.w(LOG_TAG, apiException)
            }

            airingForecast?.let {
                _airingForecastLiveData.postValue(airingForecast!!)
            }
            return@withContext airingForecast
        }
    }

    companion object {
        private const val LOG_TAG = "AiringForecastDataRepository"
    }
}