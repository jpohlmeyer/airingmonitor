package com.github.jpohlmeyer.airingmonitor.data.api

class ApiException(msg: String) : Exception(msg)