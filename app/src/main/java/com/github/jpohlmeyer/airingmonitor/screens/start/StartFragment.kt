package com.github.jpohlmeyer.airingmonitor.screens.start

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.jpohlmeyer.airingmonitor.data.model.*
import com.github.jpohlmeyer.airingmonitor.databinding.FragmentStartBinding
import com.github.jpohlmeyer.airingmonitor.helper.AiringInfoHelper
import com.github.jpohlmeyer.airingmonitor.helper.FloatFormatHelper
import com.github.jpohlmeyer.airingmonitor.screens.AppViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@AndroidEntryPoint
class StartFragment : Fragment() {

    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!

    private val viewModel: AppViewModel by activityViewModels()

    @Inject
    lateinit var floatFormatHelper: FloatFormatHelper

    @Inject
    lateinit var airingInfoHelper: AiringInfoHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.startNavGraphButton.setOnClickListener {
            val action = StartFragmentDirections.actionStartFragmentToGraphFragment()
            findNavController().navigate(action)
        }
        binding.startForecastButton.setOnClickListener {
            val action = StartFragmentDirections.actionStartFragmentToForecastFragment()
            findNavController().navigate(action)
        }
        val dataObserver = Observer<CombiListData> { newData ->
            updateData(newData)
        }
        viewModel.combiLiveData.observe(viewLifecycleOwner, dataObserver)
    }

    private fun updateData(combiListData: CombiListData) {
        var lastSensorData: SensorData? = null
        if (!combiListData.sensorData.isEmpty()) {
            lastSensorData = combiListData.sensorData.last()
        }
        var lastWindowData: WindowData? = null
        if (!combiListData.windowData.isEmpty()) {
            lastWindowData = combiListData.windowData.last()
        }
        val airingForecastData: AiringForecastContainer? = combiListData.airingForecastData
        val combiData = CombiData(lastSensorData, lastWindowData, airingForecastData)

        var ventilationCurrentText = "-"
        var ventilationCurrentColor = Color.GRAY
        var ventilationNextOptimumText = "-"
        var ventilationNextOptimumColor = Color.GRAY
        var tIndoor = "-"
        var hIndoor = "-"
        var dewIndoor = "-"
        var tOutdoor = "-"
        var hOutdoor = "-"
        var dewOutdoor = "-"
        var co2 = "-"
        var windowState = "-"
        if (combiData.sensorData != null) {
            tIndoor = floatFormatHelper.formatFloat(combiData.sensorData!!.t_indoor_scd, 1)
            hIndoor = floatFormatHelper.formatFloat(combiData.sensorData!!.h_indoor_scd, 1)
            dewIndoor = floatFormatHelper.formatFloat(combiData.sensorData!!.dewpoint_indoor, 1)
            tOutdoor = floatFormatHelper.formatFloat(combiData.sensorData!!.t_outdoor_sht, 1)
            hOutdoor = floatFormatHelper.formatFloat(combiData.sensorData!!.h_outdoor_sht, 1)
            dewOutdoor = floatFormatHelper.formatFloat(combiData.sensorData!!.dewpoint_outdoor, 1)
            co2 = floatFormatHelper.formatFloat(combiData.sensorData!!.co2, 1)
        }
        if (combiData.windowData != null) {
            windowState = combiData.windowData!!.windowstate.toString()
        }
        combiData.airingForecast?.let {
            ventilationCurrentText = airingInfoHelper.calculateAdviceText(it)
            ventilationCurrentColor = airingInfoHelper.calculateColor(it)
            val nextOptimumIndex = airingInfoHelper.calculateOptimumPosition(it)
            if (nextOptimumIndex < 1) {
                ventilationNextOptimumText = "No optimum found."
                ventilationNextOptimumColor = Color.GRAY
            } else {
                ventilationNextOptimumText = if (nextOptimumIndex == 1) {
                    "Next optimum: now"
                } else {
                    val datetime =
                        Instant.ofEpochSecond(it.airingForecast[nextOptimumIndex].timestamp)
                            .atZone(ZoneId.systemDefault()).toLocalDateTime();
                    val optimalTime = DateTimeFormatter.ofPattern("HH:mm").format(datetime)
                    "Next optimum: $optimalTime"
                }
                ventilationNextOptimumColor = airingInfoHelper.calculateColor(it, nextOptimumIndex)
            }
        }


        binding.startVentilationAdviceValue.text = ventilationCurrentText
        binding.startNextOptimumValue.text = ventilationNextOptimumText
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            binding.startLinlayout01.background.colorFilter = BlendModeColorFilter(ventilationCurrentColor, BlendMode.SRC_ATOP)
            binding.startLinlayout0.background.colorFilter = BlendModeColorFilter(ventilationNextOptimumColor, BlendMode.SRC_ATOP)
        } else {
            binding.startLinlayout01.background.setColorFilter(ventilationCurrentColor, PorterDuff.Mode.SRC_ATOP)
            binding.startLinlayout0.background.setColorFilter(ventilationNextOptimumColor, PorterDuff.Mode.SRC_ATOP)
        }
        binding.startIndoorTempValue.text =  "$tIndoor °C"
        binding.startIndoorHumValue.text =  "$hIndoor %"
        binding.startIndoorDewpointValue.text =  "$dewIndoor °C"
        binding.startOutdoorTempValue.text =  "$tOutdoor °C"
        binding.startOutdoorHumValue.text =  "$hOutdoor %"
        binding.startOutdoorDewpointValue.text =  "$dewOutdoor °C"
        binding.startCo2Value.text =  "$co2 ppm"
        binding.startWindowStateValue.text = windowState
        var lastTimestamp = 0L
        if (lastSensorData != null && lastWindowData == null) {
            lastTimestamp = lastSensorData.timestamp
        } else if (lastSensorData == null && lastWindowData != null) {
            lastTimestamp = lastWindowData.timestamp
        } else if (lastSensorData != null && lastWindowData != null) {
            lastTimestamp = if (lastSensorData.timestamp < lastWindowData.timestamp) {
                lastSensorData.timestamp
            } else {
                lastWindowData.timestamp
            }
        }
        if (lastTimestamp > 0) {
            val datetime = Instant.ofEpochSecond(lastTimestamp).atZone(ZoneId.systemDefault())
                .toLocalDateTime()
            binding.updatedText.text = "Updated: " + DateTimeFormatter.ofPattern("HH:mm:ss").format(datetime)
        }
    }

    companion object {
        private const val LOG_TAG = "StartFragment"
    }
}