package com.github.jpohlmeyer.airingmonitor.screens.forecast

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View.*
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastData
import com.github.jpohlmeyer.airingmonitor.data.persistence.ForecastAlarm
import com.github.jpohlmeyer.airingmonitor.databinding.TimelineItemBinding
import com.github.jpohlmeyer.airingmonitor.databinding.TimelinePopupBinding
import com.github.jpohlmeyer.airingmonitor.screens.AppViewModel
import com.github.vipulasri.timelineview.TimelineView
import java.text.DecimalFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter


class ForecastAdapter(
    private val context: Context,
    private val viewModel: AppViewModel,
    private var forecastData: List<AiringForecastData> = listOf()
) : RecyclerView.Adapter<ForecastViewHolder>() {

    private var currentAlarms: List<ForecastAlarm> = listOf()
    private lateinit var parent: ViewGroup
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        this.parent = parent
        val binding =
            TimelineItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ForecastViewHolder(binding, viewType)
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        val forecastArrayPosition = position + 1
        val timestamp = forecastData[forecastArrayPosition].timestamp
        val datetime =
            Instant.ofEpochSecond(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
        val datetimeString = DateTimeFormatter.ofPattern("HH:mm").format(datetime)
        holder.datetime.text = datetimeString

        val color = if (forecastData[forecastArrayPosition].total > 0) {
            Color.rgb((255 * (1 - forecastData[forecastArrayPosition].total)).toInt(), 255, 0)
        } else {
            Color.rgb(255, (255 * (1 + forecastData[forecastArrayPosition].total)).toInt(), 0)
        }


        val isMaximum = if (position == 0) {
            var nextMaxIndex = -1
            for (i in forecastArrayPosition + 1 until forecastData.size - 1) {
                if (forecastData[i].total > forecastData[i - 1].total && forecastData[i].total > forecastData[i + 1].total) {
                    nextMaxIndex = i
                    break
                }
            }
            val pos0Max = forecastData[forecastArrayPosition].total > forecastData[forecastArrayPosition - 1].total && forecastData[forecastArrayPosition].total > forecastData[forecastArrayPosition + 1].total
            pos0Max || (nextMaxIndex > 0 && forecastData[forecastArrayPosition].total >= forecastData[nextMaxIndex].total)
        } else if (forecastArrayPosition >= forecastData.size - 1) {
            false
        } else {
            (forecastData[forecastArrayPosition].total > forecastData[forecastArrayPosition - 1].total && forecastData[forecastArrayPosition].total > forecastData[forecastArrayPosition + 1].total)
        }

        val marker = getArrowMarkerFromValues(
            forecastData[forecastArrayPosition - 1].total,
            forecastData[forecastArrayPosition].total,
            isMaximum
        )
        holder.timeline.setMarker(marker, context.resources.getColor(R.color.marker_color, null))

        val safetyIndicatorMarker = getArrowMarkerFromValues(
            forecastData[forecastArrayPosition - 1].safety,
            forecastData[forecastArrayPosition].safety,
        )
        holder.safetyIndicator.setImageDrawable(safetyIndicatorMarker)
        val energyIndicatorMarker = getArrowMarkerFromValues(
            forecastData[forecastArrayPosition - 1].energy_comfort,
            forecastData[forecastArrayPosition].energy_comfort
        )
        holder.energyIndicator.setImageDrawable(energyIndicatorMarker)
        val healthIndicatorMarker = getArrowMarkerFromValues(
            forecastData[forecastArrayPosition - 1].health,
            forecastData[forecastArrayPosition].health
        )
        holder.healthIndicator.setImageDrawable(healthIndicatorMarker)

        if (currentAlarms.any { forecastAlarm -> forecastAlarm.time == timestamp }) {
            holder.reminderIcon.visibility = VISIBLE
        } else {
            holder.reminderIcon.visibility = INVISIBLE
        }

        holder.card.setCardBackgroundColor(color)
        holder.card.setOnClickListener {
            val popupBinding = TimelinePopupBinding.inflate(LayoutInflater.from(parent.context))
            val df = DecimalFormat("#.###")
            popupBinding.totalValue.text = df.format(forecastData[forecastArrayPosition].total)
            popupBinding.safetyValue.text = df.format(forecastData[forecastArrayPosition].safety)
            popupBinding.energyComfortValue.text =
                df.format(forecastData[forecastArrayPosition].energy_comfort)
            popupBinding.healthValue.text = df.format(forecastData[forecastArrayPosition].health)
            val builder = AlertDialog.Builder(context)
            val reminderButtonText = if (holder.reminderIcon.visibility != VISIBLE) {
                "Schedule Reminder"
            } else {
                "Cancel Reminder"
            }
            builder.setView(popupBinding.root).setPositiveButton(reminderButtonText) { dialog, id ->
                    viewModel.toggleReminder(timestamp)
                    if (holder.reminderIcon.visibility != VISIBLE) {
                        holder.reminderIcon.visibility = VISIBLE
                    } else {
                        holder.reminderIcon.visibility = INVISIBLE
                    }
                    dialog.cancel()
                }.setNeutralButton("Close") { dialog, id ->
                    dialog.cancel()
                }
            builder.create().show()
        }
    }

    private fun getArrowMarkerFromValues(
        previousValue: Double, currentValue: Double, isMaximum: Boolean = false
    ): Drawable {
        val slope = currentValue - previousValue
        val angle = when {
            slope > 0.1 -> 0
            slope > 0.02 -> 45
            slope > -0.02 -> 90
            slope > -0.1 -> 135
            else -> 180
        }
        val marker = when {
            angle == 0 && isMaximum -> context.resources.getDrawable(
                R.drawable.baseline_arrow_0deg_24_max, null
            )
            angle == 0 -> context.resources.getDrawable(R.drawable.baseline_arrow_0deg_24, null)
            angle == 45 && isMaximum -> context.resources.getDrawable(
                R.drawable.baseline_arrow_45deg_24_max, null
            )
            angle == 45 -> context.resources.getDrawable(R.drawable.baseline_arrow_45deg_24, null)
            angle == 90 && isMaximum -> context.resources.getDrawable(
                R.drawable.baseline_arrow_90deg_24_max, null
            )
            angle == 90 -> context.resources.getDrawable(R.drawable.baseline_arrow_90deg_24, null)
            angle == 135 && isMaximum -> context.resources.getDrawable(R.drawable.baseline_arrow_180deg_24_max, null)
            angle == 135 -> context.resources.getDrawable(R.drawable.baseline_arrow_135deg_24, null)
            isMaximum -> context.resources.getDrawable(R.drawable.baseline_arrow_180deg_24_max, null)
            else -> context.resources.getDrawable(R.drawable.baseline_arrow_180deg_24, null)
        }
        return marker
    }

    override fun getItemViewType(position: Int): Int {
        return TimelineView.getTimeLineViewType(position, itemCount)
    }

    override fun getItemCount(): Int {
        return forecastData.size - 1
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(newData: List<AiringForecastData>) {
        if (this.forecastData != newData) {
            this.forecastData = newData
            notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateReminderData(newData: List<ForecastAlarm>) {
        currentAlarms = newData
        notifyDataSetChanged()
    }

}