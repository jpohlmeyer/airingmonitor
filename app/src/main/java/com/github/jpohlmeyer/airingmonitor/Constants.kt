package com.github.jpohlmeyer.airingmonitor

object Constants {
    const val CHANNEL_ID_ALERT = "ALERT_CHANNEL"
    const val CHANNEL_ID_NOTICE = "NOTICE_CHANNEL"
    const val PERIODIC_WORKER_TAG = "PERIODIC_WORKER"
    const val WIDGET_PERIODIC_WORKER_TAG = "WIDGET_PERIODIC_WORK"
    const val REMINDER_ACTION = "REMINDER_ACTION"
    const val REMINDER_TIMESTAMP_KEY = "REMINDER_TIMESTAMP_KEY"
    const val ROOM_DATABASE = "AiringMonitorDB"
}