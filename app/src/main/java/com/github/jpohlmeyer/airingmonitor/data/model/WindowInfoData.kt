package com.github.jpohlmeyer.airingmonitor.data.model

data class WindowInfoData(
    val notify: Boolean,
    val occupied: Boolean,
    val windowstate: WindowState,
    val lastChange: Long?,
    val beforeAiringCo2: Double,
    val currentCo2: Double
)

