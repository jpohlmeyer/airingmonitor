package com.github.jpohlmeyer.airingmonitor.data.api.http

import android.content.Context
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.Volley
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton


class VolleyHTTPClient @Inject constructor(@ApplicationContext applicationContext: Context) {

    private val queue = Volley.newRequestQueue(applicationContext)

    fun <T> addRequestToQueue(request: Request<T>) {
        request.retryPolicy = DefaultRetryPolicy(20 * 1000, 1, 1.0f)
        request.tag = REQUEST_TAG
        queue.add(request)
    }

    fun cancelRequests() {
        queue.cancelAll(REQUEST_TAG)
    }

    companion object {
        private const val REQUEST_TAG = "AiringMonitorRequest"
    }
}