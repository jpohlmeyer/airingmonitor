package com.github.jpohlmeyer.airingmonitor.data.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.github.jpohlmeyer.airingmonitor.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Database(entities = [ForecastAlarm::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun forecastAlarmDao(): ForecastAlarmDao
}

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    fun provideForecastAlarmDao(appDatabase: AppDatabase): ForecastAlarmDao {
        return appDatabase.forecastAlarmDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            Constants.ROOM_DATABASE
        ).fallbackToDestructiveMigration().build()
    }
}