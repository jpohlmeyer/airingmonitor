package com.github.jpohlmeyer.airingmonitor.data.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import com.google.common.util.concurrent.ListenableFuture

@Dao
interface ForecastAlarmDao {

    @Query("SELECT * FROM forecastAlarm")
    fun getAll(): LiveData<List<ForecastAlarm>>

    @Query("SELECT * FROM forecastAlarm")
    suspend fun getAllCoroutine(): List<ForecastAlarm>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(forecastAlarm: ForecastAlarm)

    @Delete
    suspend fun delete(forecastAlarm: ForecastAlarm)

    @Query("DELETE FROM forecastAlarm WHERE time = :timestamp")
    suspend fun deleteByTimestamp(timestamp: Long)

    @Query("DELETE FROM forecastAlarm WHERE requestCode = :requestCode")
    suspend fun deleteByRequestCode(requestCode: Int)

    @Query("DELETE FROM forecastAlarm WHERE time <= :beforeTimestamp")
    suspend fun deleteOld(beforeTimestamp: Long)

}