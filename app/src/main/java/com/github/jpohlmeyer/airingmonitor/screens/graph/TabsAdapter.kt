package com.github.jpohlmeyer.airingmonitor.screens.graph

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import kotlin.reflect.full.createInstance


class TabsAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = GraphFragmentType.values().size

    override fun createFragment(position: Int): Fragment {
        return GraphFragmentType.values()[position].graphFragment.createInstance()
    }

}