package com.github.jpohlmeyer.airingmonitor.data.model

class CombiListData(
    var sensorData: List<SensorData> = listOf(),
    var windowData: List<WindowData> = listOf(),
    var airingForecastData: AiringForecastContainer? = null)