package com.github.jpohlmeyer.airingmonitor.data.persistence

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "forecastAlarm")
data class ForecastAlarm(
    @PrimaryKey val requestCode: Int,
    @ColumnInfo(name = "time") val time: Long
)
