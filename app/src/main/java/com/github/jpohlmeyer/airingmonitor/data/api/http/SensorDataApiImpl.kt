package com.github.jpohlmeyer.airingmonitor.data.api.http

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.github.jpohlmeyer.airingmonitor.R
import com.github.jpohlmeyer.airingmonitor.data.api.ApiException
import com.github.jpohlmeyer.airingmonitor.data.api.SensorDataApi
import com.github.jpohlmeyer.airingmonitor.data.model.SensorData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.suspendCancellableCoroutine
import java.lang.reflect.Type
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.typeOf

class SensorDataApiImpl @Inject constructor(
    private val volleyHTTPClient: VolleyHTTPClient,
    @ApplicationContext applicationContext: Context
) : SensorDataApi {

    private val gson = Gson()

    private val sharedPreferences: SharedPreferences

    private val settingsHubAdress: String
    private val settingsAmountLatestHours: String

    private val defaultBaseUrl: String
    private var defaultAmountLatestHours: Int

    init {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        settingsHubAdress = applicationContext.resources.getString(R.string.settings_hub_address)
        settingsAmountLatestHours = applicationContext.resources.getString(R.string.settings_hours_latest_data)

        defaultBaseUrl = applicationContext.resources.getString(R.string.default_hub_host)
        defaultAmountLatestHours = applicationContext.resources.getInteger(R.integer.default_latest_data_hours)
    }

    /**
     * Trigger request to update the sensor data.
     */
    private suspend fun updateData(from: Long, to: Long) : List<SensorData> {
        return suspendCancellableCoroutine { continuation ->
            val baseUrl = sharedPreferences.getString(settingsHubAdress, defaultBaseUrl)!!
            val url = "http://$baseUrl/sensordata?from=$from&to=$to"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                { response: String ->
                    val type: Type = TypeToken.getParameterized(List::class.java, SensorData::class.java).type
                    val sensorData: List<SensorData> = gson.fromJson(response, type)
                    continuation.resumeWith(Result.success(sensorData))
                },
                { error ->
                    val msg = if (error.networkResponse == null) {
                        "Data request failed: Network response is null"
                    } else {
                        "Data request failed: " + error.message
                    }
                    continuation.cancel(ApiException(msg))
                })
            volleyHTTPClient.addRequestToQueue(stringRequest)
        }
    }

    private suspend fun updateSingleLatestData() : SensorData {
        return suspendCancellableCoroutine { continuation ->
            val baseUrl = sharedPreferences.getString(settingsHubAdress, defaultBaseUrl)!!
            val url = "http://$baseUrl/sensordata/latest"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                { response: String ->
                    val sensorData: SensorData = gson.fromJson(response, SensorData::class.java)
                    continuation.resumeWith(Result.success(sensorData))
                },
                { error ->
                    val msg = if (error.networkResponse == null) {
                        "Data request failed: Network response is null"
                    } else {
                        "Data request failed: " + error.message
                    }
                    continuation.cancel(ApiException(msg))
                })
            volleyHTTPClient.addRequestToQueue(stringRequest)
        }
    }

    override suspend fun fetchLatestSensorData(): List<SensorData> {
        val amountLatestHours = sharedPreferences.getInt(settingsAmountLatestHours, defaultAmountLatestHours)
        val toUnixStamp = (System.currentTimeMillis() / 1000)
        val fromUnixStamp = toUnixStamp - (60 * 60 * amountLatestHours)
        return updateData(fromUnixStamp, toUnixStamp)
    }

    override suspend fun fetchSensorDataFromTo(from: Long, to: Long): List<SensorData> {
        return updateData(from, to)
    }

    override suspend fun fetchSingleLatestSensorData(): SensorData {
        return updateSingleLatestData()
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class SensorDataApiModule {
    @Binds
    abstract fun sensorDataApi(sensorDataApiImpl: SensorDataApiImpl) : SensorDataApi
}