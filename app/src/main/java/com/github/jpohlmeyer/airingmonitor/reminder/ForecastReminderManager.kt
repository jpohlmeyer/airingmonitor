package com.github.jpohlmeyer.airingmonitor.reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.jpohlmeyer.airingmonitor.Constants
import com.github.jpohlmeyer.airingmonitor.data.model.CombiListData
import com.github.jpohlmeyer.airingmonitor.data.persistence.ForecastAlarm
import com.github.jpohlmeyer.airingmonitor.data.persistence.ForecastAlarmDao
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.Instant
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ForecastReminderManager @Inject constructor(
    @ApplicationContext private val context: Context,
    private val forecastAlarmDao: ForecastAlarmDao
) {

    private val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    var reminderLiveData: LiveData<List<ForecastAlarm>> = forecastAlarmDao.getAll()

    suspend fun rescheduleAlarms() {
        withContext(Dispatchers.IO) {
            forecastAlarmDao.getAllCoroutine().let{
                for (reminder in it) {
                    setAlarm(reminder.time, reminder.requestCode)
                }
            }
        }
    }

    suspend fun toggleAlarm(timestamp: Long) {
        withContext(Dispatchers.IO) {
            val forecastAlarm =
                reminderLiveData.value?.find { forecastAlarm -> forecastAlarm.time == timestamp }
            if (forecastAlarm != null) {
                cancelAlarm(forecastAlarm)
            } else {
                var requestCode = 0
                val requestCodes = reminderLiveData.value?.map { it.requestCode }
                while (requestCodes != null && requestCode in requestCodes) {
                    requestCode = (0..1000).random()
                }
                setAlarm(timestamp, requestCode)
            }
        }
    }

    private suspend fun setAlarm(timestamp: Long, requestCode: Int) {
        val fiveMinMillis = 5 * 60 * 1000L
        val intent = Intent(context, AlarmReceiver::class.java)
        intent.action = Constants.REMINDER_ACTION
        intent.putExtra(Constants.REMINDER_TIMESTAMP_KEY, timestamp)
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_IMMUTABLE)
        alarmManager.setWindow(
            AlarmManager.RTC_WAKEUP,
            timestamp*1000 - fiveMinMillis,
            fiveMinMillis * 2,
            pendingIntent
        )
        forecastAlarmDao.insert(ForecastAlarm(requestCode = requestCode, time = timestamp))
        Log.d(
            LOG_TAG,
            "Setting reminder for timestamp $timestamp with requestCode $requestCode."
        )

    }

    private suspend fun cancelAlarm(forecastAlarm: ForecastAlarm) {
        val intent = Intent(context, AlarmReceiver::class.java)
        intent.action = Constants.REMINDER_ACTION
        intent.putExtra("KEY_FOO_STRING", "FOO")
        val pendingIntent =
            PendingIntent.getBroadcast(context, forecastAlarm.requestCode, intent, PendingIntent.FLAG_IMMUTABLE)
        alarmManager.cancel(pendingIntent)
        forecastAlarmDao.deleteByRequestCode(forecastAlarm.requestCode)
        Log.d(
            LOG_TAG,
            "Canceling reminder for timestamp ${forecastAlarm.time} with requestCode ${forecastAlarm.requestCode}."
        )
    }

    companion object {
        private const val LOG_TAG = "ForecastAlarmManager"
    }
}