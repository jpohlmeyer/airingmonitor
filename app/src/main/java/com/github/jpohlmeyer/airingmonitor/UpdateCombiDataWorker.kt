package com.github.jpohlmeyer.airingmonitor

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.github.jpohlmeyer.airingmonitor.data.model.WindowState
import com.github.jpohlmeyer.airingmonitor.data.repository.CombiDataRepository
import com.github.jpohlmeyer.airingmonitor.reminder.AlarmReceiver
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.math.abs

/**
 * Worker requests updated data
 */
@HiltWorker
class UpdateCombiDataWorker @AssistedInject constructor(
    @Assisted private val context: Context,
    @Assisted workerParams: WorkerParameters,
    private val combiDataRepository: CombiDataRepository
) :
    Worker(context, workerParams) {


    override fun doWork(): Result {
        Log.d(TAG, "Doing Work")
        runBlocking {
            withContext(Dispatchers.IO) {
                updateData()
            }
        }
        return Result.success()
    }

    private suspend fun updateData() {
        val combiListData = combiDataRepository.fetchLatestCombiListData()
        Log.d(TAG, combiListData.sensorData.toString())
    }



    companion object {
        private const val TAG = "UpdateCombiDataWorker"
    }
}