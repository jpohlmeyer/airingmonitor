package com.github.jpohlmeyer.airingmonitor

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.github.jpohlmeyer.airingmonitor.helper.PreferencesChangeListener
import com.github.jpohlmeyer.airingmonitor.widget.WidgetUpdater
import dagger.hilt.android.HiltAndroidApp
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltAndroidApp
class App: Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    @Inject
    lateinit var widgetUpdater: WidgetUpdater

    @Inject
    lateinit var openWindowNotification: OpenWindowNotification
    @Inject
    lateinit var TriFactorPreferenceListener: PreferencesChangeListener

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .setMinimumLoggingLevel(android.util.Log.DEBUG)
            .build()

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
        schedulePeriodicWork()
    }

    /**
     * configure notification channels for the app
     */
    private fun createNotificationChannel() {
        val channelAlert = NotificationChannel(Constants.CHANNEL_ID_ALERT, getString(R.string.alert_name), NotificationManager.IMPORTANCE_HIGH)
        channelAlert.description = getString(R.string.alert_desc)
        val channelNotice = NotificationChannel(Constants.CHANNEL_ID_NOTICE, getString(R.string.notice_name), NotificationManager.IMPORTANCE_DEFAULT)
        channelNotice.description  = getString(R.string.notice_desc)
        val notificationManager = applicationContext.getSystemService(
            NotificationManager::class.java
        )
        notificationManager?.createNotificationChannel(channelAlert)
        notificationManager?.createNotificationChannel(channelNotice)
    }

    /**
     * schedule a periodic work
     */
    private fun schedulePeriodicWork() {
        val uploadWorkRequest =
            PeriodicWorkRequestBuilder<UpdateCombiDataWorker>(15, TimeUnit.MINUTES)
                .addTag(Constants.PERIODIC_WORKER_TAG)
                .build()
        WorkManager
            .getInstance(this)
            .enqueueUniquePeriodicWork(Constants.PERIODIC_WORKER_TAG, ExistingPeriodicWorkPolicy.REPLACE, uploadWorkRequest)
    }

    override fun onTerminate() {
        WorkManager.getInstance(this).cancelAllWorkByTag(Constants.PERIODIC_WORKER_TAG)
        super.onTerminate()
    }
}