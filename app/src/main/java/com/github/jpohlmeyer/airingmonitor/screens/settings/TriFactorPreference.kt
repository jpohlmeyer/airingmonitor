package com.github.jpohlmeyer.airingmonitor.screens.settings

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import androidx.preference.DialogPreference
import com.github.jpohlmeyer.airingmonitor.R
import java.util.*


class TriFactorPreference(context: Context, attrs: AttributeSet? = null) :
    DialogPreference(context, attrs) {

    var safety = 1 / 3.0
    var energyComfort = 1 / 3.0
    var health = 1 / 3.0
    var dotXPos = -1.0
    var dotYPos = -1.0

    fun getFactors(): String {
        return String.format(Locale.US, "%f;%f;%f;%f;%f", safety, energyComfort, health, dotXPos, dotYPos)
    }

    fun setFactors(factors: String) {
        var localFactors = factors
        var splitFactors = localFactors.split(";")
        if (splitFactors.size != 5) {
            localFactors = getFactors()
            splitFactors = localFactors.split(";")
        }
        safety = splitFactors[0].toDouble()
        energyComfort = splitFactors[1].toDouble()
        health = splitFactors[2].toDouble()
        dotXPos = splitFactors[3].toDouble()
        dotYPos = splitFactors[4].toDouble()
        persistString(localFactors)
    }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any? {
        // Default value from attribute. Fallback value is set to 0.
        return a.getString(index)
    }

    override fun onSetInitialValue(
        defaultValue: Any?
    ) {
        // Read the value. Use the default value if it is not possible.
        setFactors(if (defaultValue == null) getPersistedString(getFactors()) else defaultValue as String)
    }

    override fun getDialogLayoutResource(): Int {
        return R.layout.pref_tri_factor
    }
}