package com.github.jpohlmeyer.airingmonitor.helper

import android.graphics.Color
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastContainer
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastData
import com.github.jpohlmeyer.airingmonitor.data.model.WindowState
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.max

@Singleton
class AiringInfoHelper @Inject constructor() {

    fun calculateColor(airingForecastContainer: AiringForecastContainer, position: Int = 1): Int {
        return if (position == 1 && airingForecastContainer.windowInfo.windowstate != WindowState.CLOSED) {
            Color.GRAY
        } else if (airingForecastContainer.airingForecast.size > position) {
            if (airingForecastContainer.airingForecast[position].total > 0) {
                Color.rgb(
                    (255 * (1 - airingForecastContainer.airingForecast[position].total)).toInt(),
                    255,
                    0
                )
            } else {
                Color.rgb(
                    255,
                    (255 * (1 + airingForecastContainer.airingForecast[position].total)).toInt(),
                    0
                )
            }
        } else {
            Color.WHITE
        }
    }

    fun calculateOptimumPosition(airingForecastContainer: AiringForecastContainer): Int {
        var maxIndex = -1
        for (i in 1 until airingForecastContainer.airingForecast.size - 1) {
            val nextMaxprevTotal = airingForecastContainer.airingForecast[i - 1].total
            val nextMaxcurrentTotal = airingForecastContainer.airingForecast[i].total
            val nextMaxnextTotal = airingForecastContainer.airingForecast[i + 1].total
            if (nextMaxcurrentTotal > nextMaxprevTotal && nextMaxcurrentTotal > nextMaxnextTotal) {
                maxIndex = i
                break
            }
        }
//        if (maxIndex > 0 && airingForecastContainer.airingForecast[maxIndex].total < airingForecastContainer.airingForecast[1].total) {
//            maxIndex = 1
//        }
        return maxIndex
    }

    fun calculateAdviceText(airingForecastContainer: AiringForecastContainer): String {
        return if (airingForecastContainer.windowInfo.windowstate != WindowState.CLOSED) {
            if (airingForecastContainer.windowInfo.lastChange != null) {
                if (airingForecastContainer.windowInfo.currentCo2 <= 770 || airingForecastContainer.windowInfo.beforeAiringCo2 <= 770) {
                    "Stop airing"
                } else {
                    val initialToGo = airingForecastContainer.windowInfo.beforeAiringCo2-770
                    val currentToGo = airingForecastContainer.windowInfo.currentCo2-770
                    val airingPercent = 100-(currentToGo/initialToGo)*100
                    val airingPercentClipped = max(airingPercent, 1.0).toInt()
                    "Airing $airingPercentClipped% done"
                }
            } else {
                "Currently airing"
            }
        } else {
            "Current state"
        }
    }
}