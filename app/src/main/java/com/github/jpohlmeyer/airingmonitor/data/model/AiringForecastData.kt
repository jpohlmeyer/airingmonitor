package com.github.jpohlmeyer.airingmonitor.data.model

data class AiringForecastData(
    val timestamp: Long,
    val safety: Double,
    val energy_comfort: Double,
    val health: Double,
    val total: Double
)