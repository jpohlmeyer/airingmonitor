package com.github.jpohlmeyer.airingmonitor.screens.settings

import android.content.DialogInterface
import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.doOnLayout
import androidx.preference.DialogPreference
import androidx.preference.PreferenceDialogFragmentCompat
import com.github.jpohlmeyer.airingmonitor.R


class TriFactorPreferenceDialogFragmentCompat : PreferenceDialogFragmentCompat() {

    lateinit var triFactorInputView: TriFactorImageView

    override fun onDialogClosed(positiveResult: Boolean) {
        if (positiveResult) {
            val factors = triFactorInputView.getFactors()
            // Get the related Preference and save the value
            val preference = preference
            if (preference is TriFactorPreference) {
                // This allows the client to ignore the user value.
                if (preference.callChangeListener(factors)) {
                    // Save the value
                    preference.setFactors(factors.joinToString(";"))
                }
            }
        }
    }

    override fun onBindDialogView(view: View) {
        super.onBindDialogView(view)

        view.doOnLayout {
            triFactorInputView = it.findViewById(R.id.tri_factor_input) as TriFactorImageView
            val preference: DialogPreference = preference
            if (preference is TriFactorPreference) {
                triFactorInputView.drawClippedDot(preference.dotXPos, preference.dotYPos)
            }
        }
    }

    companion object {
        fun newInstance(
            key: String?
        ): TriFactorPreferenceDialogFragmentCompat {
            val fragment = TriFactorPreferenceDialogFragmentCompat()
            val b = Bundle(1)
            b.putString(ARG_KEY, key)
            fragment.arguments = b
            return fragment
        }
    }
}