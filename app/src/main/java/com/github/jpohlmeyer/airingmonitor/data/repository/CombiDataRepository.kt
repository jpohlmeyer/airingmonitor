package com.github.jpohlmeyer.airingmonitor.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.jpohlmeyer.airingmonitor.data.api.AiringForecastApi
import com.github.jpohlmeyer.airingmonitor.data.api.ApiException
import com.github.jpohlmeyer.airingmonitor.data.api.SensorDataApi
import com.github.jpohlmeyer.airingmonitor.data.api.WindowDataApi
import com.github.jpohlmeyer.airingmonitor.data.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CombiDataRepository @Inject constructor(
    private val sensorDataApi: SensorDataApi,
    private val windowDataApi: WindowDataApi,
    private val airingForecastApi: AiringForecastApi
) {

    private val _combiLiveData: MutableLiveData<CombiListData> = MutableLiveData()
    val combiLiveData: LiveData<CombiListData>
        get() = _combiLiveData

    private var combiListData = CombiListData()

    suspend fun fetchLatestCombiListData(): CombiListData {
        return withContext(Dispatchers.IO) {
            var sensorData: List<SensorData>? = null
            var windowData: List<WindowData>? = null
            var airingForecastData: AiringForecastContainer? = null
            try {
                sensorData = sensorDataApi.fetchLatestSensorData()
            } catch (apiException: ApiException) {
                Log.w(LOG_TAG, "Refresh of sensordata failed with Exception:")
                Log.w(LOG_TAG, apiException)
            }
            try {
                windowData = windowDataApi.fetchLatestWindowData()
            } catch (apiException: ApiException) {
                Log.w(LOG_TAG, "Refresh of windowdata failed with Exception:")
                Log.w(LOG_TAG, apiException)
            }
            try {
                airingForecastData = airingForecastApi.fetchAiringForecast()
            } catch (apiException: ApiException) {
                Log.w(LOG_TAG, "Refresh of airingInfoData failed with Exception:")
                Log.w(LOG_TAG, apiException)
            }

            if (sensorData != null) {
                combiListData.sensorData = sensorData
            }
            if (windowData != null) {
                combiListData.windowData = windowData
            }
            if (airingForecastData != null) {
                combiListData.airingForecastData = airingForecastData
            }

            _combiLiveData.postValue(combiListData)
            return@withContext combiListData
        }
    }

    companion object {
        private const val LOG_TAG = "CombiDataRepository"
    }
}