package com.github.jpohlmeyer.airingmonitor.screens.forecast

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.jpohlmeyer.airingmonitor.databinding.TimelineItemBinding
import com.github.vipulasri.timelineview.TimelineView
import com.google.android.material.card.MaterialCardView

class ForecastViewHolder(binding: TimelineItemBinding, viewType: Int) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.timeline.initLine(viewType)
    }
    val datetime: TextView = binding.datetime
    val timeline: TimelineView = binding.timeline
    val card: MaterialCardView = binding.card
    val safetyIcon: ImageView = binding.safetyIcon
    val safetyIndicator: ImageView = binding.safetyIndicator
    val energyIcon: ImageView = binding.energyIcon
    val energyIndicator: ImageView = binding.energyIndicator
    val healthIcon: ImageView = binding.healthIcon
    val healthIndicator: ImageView = binding.healthIndicator
    val reminderIcon: ImageView = binding.reminderIcon
}