package com.github.jpohlmeyer.airingmonitor.screens

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.jpohlmeyer.airingmonitor.data.model.AiringForecastData
import com.github.jpohlmeyer.airingmonitor.data.model.CombiListData
import com.github.jpohlmeyer.airingmonitor.data.persistence.ForecastAlarm
import com.github.jpohlmeyer.airingmonitor.data.repository.AiringForecastDataRepository
import com.github.jpohlmeyer.airingmonitor.data.repository.CombiDataRepository
import com.github.jpohlmeyer.airingmonitor.reminder.ForecastReminderManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AppViewModel @Inject constructor(
    private val combiDataRepository: CombiDataRepository,
    private val airingForecastDataRepository: AiringForecastDataRepository,
    private val forecastReminderManager: ForecastReminderManager
) : ViewModel() {

    val combiLiveData: LiveData<CombiListData>
        get() = combiDataRepository.combiLiveData
//
//    val forecastLiveData: LiveData<List<AiringForecastData>>
//        get() = airingForecastDataRepository.airingForecastLiveData

    val reminderLiveData: LiveData<List<ForecastAlarm>>
        get() = forecastReminderManager.reminderLiveData

    init {
        updateData()
//        updateForecastData()
    }


    fun updateData() {
        viewModelScope.launch {
            combiDataRepository.fetchLatestCombiListData()
            Log.d(LOG_TAG, "ViewModel updated data")
        }
    }
//
//    fun updateForecastData() {
//        viewModelScope.launch {
//            airingForecastDataRepository.fetchAiringForecastData()
//            Log.d(LOG_TAG, "ViewModel updated forecast data")
//        }
//    }

    fun toggleReminder(timestamp: Long) {
        viewModelScope.launch {
            forecastReminderManager.toggleAlarm(timestamp)
        }
    }

    companion object {
        private const val LOG_TAG = "AppViewModel"
    }
}